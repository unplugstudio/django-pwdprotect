<a name="1.0.1"></a>
## [1.0.1](https://gitlab.com/unplugstudio/django-pwdprotect/compare/v1.0.0...v1.0.1) (2018-08-21)


### Bug Fixes

* make compatible with Django >1.10 ([99167d7](https://gitlab.com/unplugstudio/django-pwdprotect/commit/99167d7))



<a name="1.0.0"></a>
# 1.0.0 (2018-06-12)


### Features

* add username to PasswordProtectedUrl ([9005804](https://gitlab.com/unplugstudio/django-pwdprotect/commit/9005804))



