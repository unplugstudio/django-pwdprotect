from django.apps import AppConfig


class PwdProtectConfig(AppConfig):
    name = "pwdprotect"
    verbose_name = "Protect"
